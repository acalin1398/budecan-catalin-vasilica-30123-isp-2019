package budecan.catalin.vasilica.lab4.ex4;

import budecan.catalin.vasilica.lab4.ex2.Author;

public class Book2 extends Author {
    private String name;
    private double price;
    private Author[] authors;
    private int qtyInStock = 0;

    public Book2(String name, String email, char gender, String name1, double price, Author[] authors) {
        super(name, email, gender);
        this.name = name1;
        this.price = price;
        this.authors = authors;
    }

    public Book2(String name, String email, char gender, String name1, double price, Author[] authors, int qtyInStock) {
        super(name, email, gender);
        this.name = name1;
        this.price = price;
        this.authors = authors;
        this.qtyInStock = qtyInStock;
    }

    /*@Override
    public String getName() {
        return name;
    }*/

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
        System.out.println("book-" + getName() + " by " + getAuthors());
        return null;
    }

    public void printAuthors() {
        for (int i = 0; i <= getAuthors().length; i++) System.out.println(getAuthors()[i].getName());
    }
}
